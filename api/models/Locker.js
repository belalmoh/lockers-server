/**
 * Locker.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  connection: 'WadiDeglaLockers',
  tableName: 'locker',

  attributes: {
    id: {
      type: 'integer',
      autoIncrement: true,
      primaryKey: true
    },
    number: {
      type: 'integer',
      autoIncrement: true,
      unique: true
    },
    user_name: {
      type: 'string'
    }
  }
};

