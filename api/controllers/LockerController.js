/**
 * LockerController
 *
 * @description :: Server-side logic for managing lockers
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

async function getAllLockers(){
  return new Promise(async (resolve, reject) => {
    try {
      return resolve(Locker.find({}))
    } catch (err){
      reject(err)
    }
  })
}

async function addLocker({userName, userLockerNumber}){
  return new Promise(async (resolve, reject) => {
    try {
      const addedLocker = await Locker.create({user_name: userName, number: userLockerNumber});
      return resolve(Locker.find({}))
    } catch (err){
      reject(err)
    }
  })
}
// create the database !
module.exports = {
	add: (async function(req, res){
    
    const {userName, userLockerNumber} = req.body;
    const AddedLocker = addLocker({userName, userLockerNumber});

    return res.json(addLocker);
  }),

  getAll: (async function(req, res){
    const AllLockers = await getAllLockers();
    return res.json(AllLockers)
  })
};

