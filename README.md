# lockers-server


## Steps for making things work:

```sh
$ npm install sails -g
$ git clone https://gitlab.com/belalmoh/lockers-server.git
$ cd lockers-server
$ yarn 
```

1- **Please make sure that you have MySQL installed on your PC**

2- **Also to make things work, create database with name**: *wadidegla_lockers*

3- if you have different MySQL instance credentials, navigate to **config/connections.js** and find **WadiDeglaLockers -> Line 43**, there you can customize your credentials based on yours.


### After you're done with the above steps, type: 
```sh
$ sails lift
```
Now you're ready to use that server instance.